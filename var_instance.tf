provider "aws" {
     region = "us-west-1"
     shared_config_files      = ["/root/.aws/configure"]
     shared_credentials_files = ["/root/.aws/credentials"]
 }
 resource "aws_instance" "instance2" {
   ami = var.ami
   instance_type = var.instance_type
   key_name = var.key_name
   tags = var.tags
 }