variable "ami" {
  default = "ami-02bfcfbf6fc7e8ce4"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "key_name" {
  default = "california"
}

variable "tags" {
  default = {
    name = "my_instance"
  }
}